#!/usr/bin/env python3
# Generic source resolver for scifolder.
# A. Schlemmer, 05/2022

import os
import re
import caosdb as db
from caoscrawler.utils import has_parent
from caoscrawler.stores import GeneralStore, RecordStore
from caoscrawler.converters import Converter, TextElementConverter, create_records
from caoscrawler.structure_elements import (StructureElement, Directory, File,
                                            TextElement, DictTextElement, DictListElement)
from typing import Optional, Union
from abc import abstractmethod
import re

import yaml_header_tools

import glob


class SourceResolver(TextElementConverter):
    """
    This resolver uses a source list element (e.g. from the markdown readme file)
    to link sources correctly.
    """

    def __init__(self, definition: dict, name: str,
                 converter_registry: dict):
        """
        Initialize a new directory converter.
        """
        super().__init__(definition, name, converter_registry)

    def create_children(self, generalStore: GeneralStore,
                        element: StructureElement):
        # The source resolver does not create children:
        return []

    def create_records(self, values: GeneralStore,
                       records: RecordStore,
                       element: StructureElement,
                       file_path_prefix: str):
        if not isinstance(element, TextElement):
            raise RuntimeError()

        # This function must return a list containing tuples, each one for a modified
        # property: (name_of_entity, name_of_property)
        keys_modified = []

        # This is the name of the entity where the source is going to be attached:
        attach_to_scientific_activity = self.definition["scientific_activity"]
        rec = records[attach_to_scientific_activity]

        # The "source" is a path to a source project, so it should have the form:
        # /<Category>/<project>/<scientific_activity>/
        # obtain these information from the structure element:
        val = element.value
        regexp = (r'/(?P<category>(SimulationData)|(ExperimentalData)|(DataAnalysis))'
                  '/(?P<project_date>.*?)_(?P<project_identifier>.*)'
                  '/(?P<date>[0-9]{4,4}-[0-9]{2,2}-[0-9]{2,2})(_(?P<identifier>.*))?/')

        res = re.match(regexp, val)
        if res is None:
            raise RuntimeError("Source cannot be parsed correctly.")

        # Mapping of categories on the file system to corresponding record types in CaosDB:
        cat_map = {
            "SimulationData": "Simulation",
            "ExperimentalData": "DOCDataset",
            "DataAnalysis": "DataAnalysis"}
        linkrt = cat_map[res.group("category")]

        keys_modified.extend(create_records(values, records, {
            "Project": {
                "date": res.group("project_date"),
                "identifier": res.group("project_identifier"),
            },
            linkrt: {
                "date": res.group("date"),
                "identifier": res.group("identifier"),
                "project": "$Project"
            },
            attach_to_scientific_activity: {
                "sources": "+$" + linkrt
            }}, file_path_prefix))

        # Process the records section of the yaml definition:
        keys_modified.extend(
            super().create_records(values, records, element, file_path_prefix))

        return keys_modified
