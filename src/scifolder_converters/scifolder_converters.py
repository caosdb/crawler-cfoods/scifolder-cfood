#!/usr/bin/env python3
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2021 Henrik tom Wörden
#               2021 Alexander Schlemmer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

import os
import re
from .utils import has_parent
from .stores import GeneralStore, RecordStore
from .structure_elements import (StructureElement, Directory, File,
                                 TextElement, DictTextElement, DictListElement)
import yaml_header_tools

from newcrawler.converters import Converter


class ScifolderMarkdownFileConverter(Converter):
    def __init__(self, definition: dict, name: str):
        """
        Initialize a new directory converter.
        """
        super().__init__(definition, name)

    def create_children(self, generalStore: GeneralStore,
                        element: StructureElement):
        if not isinstance(element, File):
            raise RuntimeError("A markdown file is needed to create children.")

        header = yaml_header_tools.get_header_from_file(element.path, clean=False)
        children: list[StructureElement] = []

        for name, entry in header.items():
            if type(entry) == list:
                children.append(DictListElement(name, entry))
            elif type(entry) == str:
                children.append(DictTextElement(name, entry))
            else:
                raise RuntimeError("Header entry {} has incompatible type.".format(name))
        return children

    def typecheck(self, element: StructureElement):
        return isinstance(element, File)

    def match(self, element: StructureElement):
        if not isinstance(element, File):
            raise RuntimeError("Element must be a file.")
        m = re.match(self.definition["match"], element.name)
        if m is None:
            return None
        try:
            yaml_header_tools.get_header_from_file(element.path)
        except yaml_header_tools.NoValidHeader:
            return None
        return m.groupdict()
